package lollyShopSystem;

public class Employee extends Person {
    double hourlyWorked;
    String employeeId;

    public Employee() {
        super();
        this.hourlyWorked = 0;
        this.employeeId = "";
    }


    public Employee(String firstName, String surname, String phoneNumber, double hourlyWorked, String employeeId) {
        super(firstName, surname, phoneNumber);
        this.hourlyWorked = hourlyWorked;
        this.employeeId = employeeId;
        this.setDiscount();
    }

    public double getHourlyWorked() {
        return hourlyWorked;
    }

    public void setHourlyWorked(double hourlyWorked) {
        this.hourlyWorked = hourlyWorked;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public void setDiscount(){
        if (hourlyWorked>30){
            this.discount = 15;
        }
        else if(hourlyWorked>20){
            this.discount = 10;
        }
        else {
            this.discount = 5;
        }
    }

    @Override
    public String toString() {
        return firstName+" "+surname+"\n"+"Contact number: "+phoneNumber+"\nEmployee Id: "+employeeId+"\nhas worked "+hourlyWorked+" hours and will get "+discount+"% discount";
    }

}
