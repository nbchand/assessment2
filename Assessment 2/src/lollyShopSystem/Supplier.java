package lollyShopSystem;

public class Supplier extends Person{
    String companyName;
    String supplierStatus;
    String branch;

    public Supplier() {
        super();
        this.companyName="";
        this.supplierStatus="";
        this.branch="";
    }

    public Supplier(String firstName, String surname, String phoneNumber, String companyName, String supplierStatus, String branch) {
        super(firstName, surname, phoneNumber);
        this.companyName = companyName;
        this.supplierStatus = supplierStatus.toLowerCase();
        this.branch = branch;
        this.setDiscount();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSupplierStatus() {
        return supplierStatus;
    }

    public void setSupplierStatus(String supplierStatus) {
        this.supplierStatus = supplierStatus.toLowerCase();
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public void setDiscount(){
        if(supplierStatus.equals("active")){
            discount = 15;
        }
        else if(supplierStatus.equals("future")){
            discount = 5;
        }
        else {
            discount = 10;
        }
    }

    @Override
    public String toString() {
        return firstName+" "+surname+"\n"+"Contact number: "+phoneNumber+"\nCompany Name: "+companyName+"\nBranch: "+branch+"\nis "+(supplierStatus.equals("active")?"an":"a")+" "+supplierStatus+" supplier and will get "+discount+"% discount";
    }

}
