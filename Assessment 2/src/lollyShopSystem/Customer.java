package lollyShopSystem;

public class Customer extends Person {
    String emailAddress;
    String paymentMethod;

    public Customer() {
        super();
        this.emailAddress ="";
        this.paymentMethod ="";
    }

    public Customer(String firstName, String surname, String phoneNumber,String emailAddress, String paymentMethod) {
        super(firstName, surname, phoneNumber);
        this.emailAddress = emailAddress;
        this.paymentMethod = paymentMethod;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @Override
    public String toString() {
        return firstName+" "+surname+"\n"+"Contact number: "+phoneNumber+"\nEmail address: "+emailAddress+"\nPayment method: "+paymentMethod+"\nis a customer and will not get any discount";
    }
}

