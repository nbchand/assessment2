package lollyShopSystem;

public class LollyShopDriver {
    public static void main(String[] args) {

        Employee employee1 = new Employee("John","Morrison","1234567890",23,"112");

        Employee employee2 = new Employee();
        employee2.setFirstName("Rhea");
        employee2.setSurname("Smith");
        employee2.setPhoneNumber("0321654987");
        employee2.setEmployeeId("113");
        employee2.setHourlyWorked(35);
        employee2.setDiscount();


        Customer customer1 = new Customer("Kate","Winslet","0463621978","kate321@gmail.com","cash");

        Customer customer2 = new Customer();
        customer2.setFirstName("Jason");
        customer2.setSurname("Holder");
        customer2.setPhoneNumber("8321655987");
        customer2.setEmailAddress("jason33@gmail.com");
        customer2.setPaymentMethod("Online Payment");

        Supplier supplier1 = new Supplier("Kumar","Shrestha","6546622189","CG Group","active","Melbourne");

        Supplier supplier2 = new Supplier();
        supplier2.setFirstName("Lilly");
        supplier2.setSurname("Styles");
        supplier2.setPhoneNumber("8321005987");
        supplier2.setCompanyName("Orion Technologies");
        supplier2.setSupplierStatus("Past");
        supplier2.setBranch("New York");
        supplier2.setDiscount();

        System.out.println("Welcome to Lolly shop System developed by 321 Ramesh Baral\n");
        System.out.println("**************************Employee details**************************\n");
        System.out.println(employee1+"\n");
        System.out.println(employee2+"\n");
        System.out.println("**************************Customer details**************************\n");
        System.out.println(customer1+"\n");
        System.out.println(customer2+"\n");
        System.out.println("**************************Supplier details**************************\n");
        System.out.println(supplier1+"\n");
        System.out.println(supplier2+"\n");
    }
}
